from pathlib import Path

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import scipy.interpolate

MATERIALS_PATH = Path('C:/dev/empy/materials')

def load_material(material_name):
    path_to_n = MATERIALS_PATH / (material_name + '_n.csv')
    path_to_k = MATERIALS_PATH / (material_name + '_k.csv')

    n_table = pd.read_csv(path_to_n, comment='#')
    k_table = pd.read_csv(path_to_k, comment='#')

    return Material(
        scipy.interpolate.interp1d(1000 * n_table['wl'], n_table['n'], kind='cubic'),
        scipy.interpolate.interp1d(1000 * k_table['wl'], k_table['k'], kind='cubic')
    )

class Material:
    def __init__(self, n_interp, k_interp):
        self.n_interp = n_interp
        self.k_interp = k_interp

    def get_ri(self, wavelength):
        return self.n_interp(wavelength) + 1j * self.k_interp(wavelength)

class ConstantMaterial:
    def __init__(self, ri):
        self.ri = ri

    def get_ri(self, wavelength = 0):
        return self.ri