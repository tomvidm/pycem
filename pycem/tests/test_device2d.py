import numpy as np
import itertools
import pandas as pd

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

from pycem.device2d import (
    Device2D,
    Layer2D,
    HomogenousLayer2D,
    draw_circle,
    draw_rectangle,
    get_wave_vector_expansion,
    get_harmonic_indices
)

from pycem.material import (
    ConstantMaterial,
    load_material
)

air = ConstantMaterial(1.0)
mat = ConstantMaterial(2.0)
glass = load_material('N-BK7')
silicon = load_material('Aspnes-31.5')

def test_wave_vector_expansion_sanity_check():
    (kx, ky, kzr, kzt, total_harmonics, harmonic_indexes) = get_wave_vector_expansion(0, 0, 250, (500, 500), air, air, (4, 4))

    assert(total_harmonics == 81)
    assert(kx.shape == (9, 9))
    assert(ky.shape == (9, 9))
    assert(kzr.shape == (9, 9))
    assert(kzt.shape == (9, 9))
    assert(harmonic_indexes.shape == (9, 9))

def test_convolution_matrix_generation():
    texture = np.ones((256, 256))
    layer = Layer2D(0, texture, {1: mat}, (1, 1))
    eps = layer.get_convolution_matrix(0, (4, 4))

    expected_eps = 4.0 * np.identity(81)

    assert(np.sum(np.isclose(eps, expected_eps)) == 81 * 81)

def test_energy_conservation_for_single_layer_diffraction_efficiencies():
    device_period_profile = (1000, 1000)
    real_space_resolution = (512, 512)
    max_harmonics = (6, 6)

    texture = draw_circle(0.9, real_space_resolution)
    reflection_region   = HomogenousLayer2D(1e12, glass)
    layer               = Layer2D(500, texture, {1: silicon, 0: glass}, device_period_profile)
    transmission_region = HomogenousLayer2D(1e12, glass)

    x, y, harmonics_x, harmonics_y, total_harmonics, harmonic_indexes = get_harmonic_indices(max_harmonics)

    eps0 = layer.get_convolution_matrix(300, max_harmonics)
    
    theta_phi_wavelength = itertools.product(np.arange(34, 89), [60], [450])

    r = {(0, 0): [], (-1, 0): [], (1, 0): []}
    t = {(0, 0): [], (-1, 0): [], (1, 0): []}
    for (theta, phi, wavelength) in theta_phi_wavelength:
        fields = layer.get_field_components_from_incident_wave(theta * np.pi / 180, phi * np.pi / 180, wavelength, reflection_region, transmission_region, max_harmonics)
        print(theta)
        X = np.ravel(x)
        Y = np.ravel(y)
        Z = np.zeros(x.size)

        e_r_s   = np.real(np.linalg.norm(fields['reflected']['E']['s_polarized'], axis = 1))[harmonic_indexes]
        e_r_p   = np.real(np.linalg.norm(fields['reflected']['E']['p_polarized'], axis = 1))[harmonic_indexes]
        e_r_avg = np.real(np.linalg.norm(fields['reflected']['E']['avg'], axis = 1))[harmonic_indexes]
        R       = fields['reflected']['E']['avg_eff'][harmonic_indexes]

        e_t_s   = np.real(np.linalg.norm(fields['transmitted']['E']['s_polarized'], axis = 1))[harmonic_indexes]
        e_t_p   = np.real(np.linalg.norm(fields['transmitted']['E']['p_polarized'], axis = 1))[harmonic_indexes]
        e_t_avg = np.real(np.linalg.norm(fields['transmitted']['E']['avg'], axis = 1))[harmonic_indexes]
        T       = fields['transmitted']['E']['avg_eff'][harmonic_indexes]

        R_sum = np.sum(R)
        T_sum = np.sum(T)
        R       = R / R_sum
        T       = T / T_sum
        r[(0, 0)].append(R[6,6])
        r[(-1, 0)].append(R[5,6])
        r[(1, 0)].append(R[7,6])
        t[(0, 0)].append(T[6,6])
        t[(-1, 0)].append(T[5,6])
        t[(1, 0)].append(T[7,6])

    df = pd.DataFrame(columns = ['r(0,0)', 'r(-1,0)', 'r(1,0)', 't(0,0)', 't(-1,0)', 't(1,0)'])
    df['r(0,0)'] = r[(0,0)]
    df['r(-1,0)'] = r[(-1,0)]
    df['r(1,0)'] = r[(1,0)]
    df['t(0,0)'] = t[(0,0)]
    df['t(-1,0)'] = t[(-1,0)]
    df['t(1,0)'] = t[(1,0)]

    df.to_csv('hello.csv')
        # First sanity check
        #assert(R_sum + T_sum <= 1)

        #£fig, axes = plt.subplots(2, 4)
        #£axes[0,0].imshow(e_r_s)
        #£axes[0,1].imshow(e_r_p)
        #£axes[0,2].imshow(e_r_avg)
        #£axes[0,3].imshow(R)
        #£axes[1,0].imshow(e_t_s)
        #£axes[1,1].imshow(e_t_p)
        #£axes[1,2].imshow(e_t_avg)
        #£axes[1,3].imshow(R)
        #£#plt.show()
        #£plt.savefig('{}_{}_{}.png'.format(theta, phi, wavelength))
        #£plt.close()
#
        

def te22st_device2d_diffraction_efficiencies_energy_conservation():
    # Send an incident wave of wavelength 520 normal to the grating.
    # Check that the 0,0 order reflects less than 100% of the incoming energy.
    device_period_profile = (500, 500)

    real_space_resolution = (512, 512)
    
    texture_layer_0 = draw_circle(0.3, real_space_resolution)
    texture_layer_1 = draw_circle(0.5, real_space_resolution)
    texture_layer_2 = draw_circle(0.7, real_space_resolution)
    texture_layer_3 = draw_circle(0.9, real_space_resolution)

    reflection_region   = HomogenousLayer2D(1e12, air)
    layer_0             = Layer2D(50, texture_layer_0, {0: glass, 1: silicon}, device_period_profile)
    layer_1             = Layer2D(50, texture_layer_1, {0: glass, 1: silicon}, device_period_profile)
    layer_2             = Layer2D(50, texture_layer_2, {0: glass, 1: silicon}, device_period_profile)
    layer_3             = Layer2D(50, texture_layer_3, {0: glass, 1: silicon}, device_period_profile)
    transmission_region = HomogenousLayer2D(1e12, glass)

    device = Device2D([layer_0, layer_1, layer_2, layer_3], device_period_profile)

    R, T = device.get_diffraction_efficiencies( 
        0 * np.pi / 180, 0, 300, reflection_region, transmission_region, (4, 4))

    R_sum = np.sum(R)
    T_sum = np.sum(T)

    # Sanity checks
    assert(np.sum(T) > 0)
    assert(np.sum(R) < 1)

    # Conservation of energy is pretty neat too
    assert(0.9999 < R_sum + T_sum < 1.0001)
