import time

from pathlib import Path
from math import cos, sin

import numpy as np
import pandas as pd
from scipy.sparse import (
    issparse,
    csr_matrix
)
import matplotlib.pyplot as plt

from pycem.material import (
    ConstantMaterial,
    load_material
)

def get_wave_vector_expansion(theta, phi, wavelength, period_profile, material_refl, material_trn, max_harmonics):
    """
    parameters:
    theta: angle between the incident wavefront and the xy plane
    phi: angle between the incident wavefront and the xz plane
    wavelength: wavelength of the incident wavefront
    material_refl: the reflection region material
    material_trn: the transmission region material
    max_harmonics: the number of harmonics to compute (-n, n),(-m, m)

    NOTE: For some combinations of arguments, there will be zero valued entries in 
          one of more of kx, ky, kz_refl, kz_tr. Why is it? Find a solution to this,
          as creating the A and B matrices for the ETM approach raises a singular matrix error,
          due to the expressions like (kx * rx + ky * ry) \ kzr
    """
    period_x, period_y = period_profile
    x, y, _, _, total_harmonics, harmonic_indices = get_harmonic_indices(max_harmonics)

    ri_refl = material_refl.get_ri(wavelength)
    ri_trn  = material_trn.get_ri(wavelength)

    kx      = ri_refl * sin(theta) * cos(phi) - x * (wavelength / period_x)
    ky      = ri_refl * sin(theta) * sin(phi) - y * (wavelength / period_y)
    kz_refl = -np.conj(np.lib.scimath.sqrt(np.power(ri_refl, 2) - np.power(kx, 2) - np.power(ky, 2)))
    kz_trn  =  np.conj(np.lib.scimath.sqrt(np.power(ri_trn,  2) - np.power(kx, 2) - np.power(ky, 2)))

    return (kx, ky, kz_refl, kz_trn, total_harmonics, harmonic_indices)

def get_harmonic_indices(max_harmonics):
    max_harmonics_x, max_harmonics_y = max_harmonics
    harmonics_x = np.arange(-max_harmonics_x, max_harmonics_x + 1)
    harmonics_y = np.arange(-max_harmonics_y, max_harmonics_y + 1)
    total_harmonics = harmonics_x.size * harmonics_y.size
    x, y = np.meshgrid(harmonics_x, harmonics_y)
    harmonic_indexes = x + harmonics_x.size * y + total_harmonics // 2

    return x, y, harmonics_x, harmonics_y, total_harmonics, harmonic_indexes

def get_k0(wavelength):
    return 2 * np.pi / wavelength

class Device2D:
    def __init__(self, layers, period_profile):
        self.layers = layers
        self.period_profile = period_profile

    def get_diffraction_efficiencies(self, theta, phi, wavelength, r_region, t_region, max_harmonics):
        """
        Get diffractive efficiencies using the enhanced transmittance matrix approach

        NOTE: Assumes that the magnetic permeability is always 1.
        """
        kx, ky, kzr, kzt, total_harmonics, harmonic_indexes = self.get_wave_vector_expansion(theta, phi, wavelength, r_region, t_region, max_harmonics)
        
        A, B = get_etm_A_and_B(kx, ky, kzr, kzt)

        a_cache = {}
        b_cache = {}
        X_cache = {}
        F_cache = {}

        last_layer = self.layers[len(self.layers) - 1]

        # Handle last layer separately from the intermediate layers
        W, V, lam = last_layer.get_eigenmodes(theta, phi, wavelength, r_region, t_region, max_harmonics)
        F = get_etm_F_matrix(W, V)

        a, b = np.split(np.linalg.solve(get_etm_F_matrix(W, V), B), 2, axis = 0)
        a_cache[len(self.layers) - 1] = a
        b_cache[len(self.layers) - 1] = b
        X_cache[len(self.layers) - 1] = np.matmul(W, np.matmul(np.diag(np.exp(-get_k0(wavelength) * lam * last_layer.height)), np.linalg.inv(W)))
        F_cache[len(self.layers) - 1] = F

        # Handle the intermediate layers with annoying indexing
        for i in range(len(self.layers) - 2, -1, -1):
            print('Layer {}/{} - backwards stepping'.format(i + 1, len(self.layers)))
            layer = self.layers[i]

            W, V, lam = layer.get_eigenmodes(theta, phi, wavelength, r_region, t_region, max_harmonics)
            F = get_etm_F_matrix(W, V)
            F_cache[i] = F
            X_cache[i] = np.matmul(W, np.matmul(np.diag(np.exp(-get_k0(wavelength) * lam * layer.height)), np.linalg.inv(W)))

            prev_F = F_cache[i + 1]
            prev_X = X_cache[i + 1]
            prev_a = a_cache[i + 1]
            prev_b = b_cache[i + 1]

            IX = np.concatenate([
                np.concatenate([
                    np.identity(2 * total_harmonics), 
                    np.zeros((2 * total_harmonics, 2 * total_harmonics)),
                ], axis = 1),
                np.concatenate([
                    np.zeros((2 * total_harmonics, 2 * total_harmonics)),
                    prev_X
                ], axis = 1)
            ], axis = 0)
            IbaX = np.concatenate([
                np.identity(2 * total_harmonics),
                np.matmul(prev_b, np.linalg.solve(prev_a, prev_X))
            ], axis = 0)

            a, b = np.split(np.matmul(np.linalg.solve(F, prev_F), np.matmul(IX, IbaX)), 2, axis = 0)
            a_cache[i] = a
            b_cache[i] = b
    
        # Initialize unit source vector
        p_beta = 0 * np.pi / 180
        px, py, pz = 1, 0, 0

        kx_inc = sin(theta) * cos(phi)
        ky_inc = sin(theta) * sin(phi)
        kz_inc = cos(theta)

        s = np.zeros((4 * total_harmonics), dtype = complex)
        s[0 * total_harmonics + total_harmonics // 2] = px
        s[1 * total_harmonics + total_harmonics // 2] = py
        s[2 * total_harmonics + total_harmonics // 2] = 1j * (py * kz_inc - pz * ky_inc)
        s[3 * total_harmonics + total_harmonics // 2] = 1j * (pz * kx_inc - px * kz_inc)

        IX = np.concatenate([
            np.concatenate([
                np.identity(2 * total_harmonics), 
                np.zeros((2 * total_harmonics, 2 * total_harmonics)),
            ], axis = 1),
            np.concatenate([
                np.zeros((2 * total_harmonics, 2 * total_harmonics)),
                X_cache[0]
            ], axis = 1)
        ], axis = 0)

        IbaX = np.concatenate([
            np.identity(2 * total_harmonics),
            np.matmul(b_cache[0], np.linalg.solve(a_cache[0], X_cache[0]))
        ], axis = 0)

        B_bar = np.matmul(F_cache[0], np.matmul(IX, IbaX))

        AB = np.concatenate([-1 * A, B_bar], axis = 1)
        r, t = np.split(np.linalg.solve(AB, s), 2)

        for i, _ in enumerate(self.layers[1:]):
            t = np.matmul(a_cache[i], np.linalg.solve(X_cache[i], t))
        rx, ry = np.split(r, 2)
        tx, ty = np.split(t, 2)

        kx = np.ravel(kx)
        ky = np.ravel(ky)
        kzr = np.ravel(kzr)
        kzt = np.ravel(kzt)

        rz = -np.divide(np.multiply(kx, rx) + np.multiply(ky, ry), kzr)
        tz = -np.divide(np.multiply(kx, tx) + np.multiply(ky, ty), kzr)

        r = np.power(np.abs(rx), 2) + np.power(np.abs(ry), 2) + np.power(np.abs(rz), 2)
        t = np.power(np.abs(tx), 2) + np.power(np.abs(ty), 2) + np.power(np.abs(tz), 2)

        R = np.multiply(r, np.real(-kzr)) / np.real(kz_inc)
        T = np.multiply(t, np.real(kzt)) / np.real(kz_inc)

        return R[harmonic_indexes], T[harmonic_indexes]

    def get_wave_vector_expansion(self, theta, phi, wavelength, r_region, t_region, max_harmonics):
        return get_wave_vector_expansion(theta, phi, wavelength, self.period_profile, r_region.material, t_region.material, max_harmonics)

def get_etm_F_matrix(W, V):
    """
    Returns the F matrix as defined in slide 25, in the description
    of the enhanced transmittance method
    source: https://empossible.net/wp-content/uploads/2019/08/Lecture-7c-RCWA-Extras.pdf
    """
    return np.concatenate([
        np.concatenate([ W, W], axis = 1),
        np.concatenate([-V, V], axis = 1)
    ], axis = 0)

def get_etm_A_and_B(kx, ky, kzr, kzt):
    """
    Returns the A and B matrices as defined in slide 25, in the description
    of the enhanced transmittance method
    source: https://empossible.net/wp-content/uploads/2019/08/Lecture-7c-RCWA-Extras.pdf

    NOTE: Assumes that the magnetic permeability constant is approximately unity.
    """
    kxkx  = np.ravel(np.power(kx, 2))
    kxky  = np.ravel(np.multiply(kx, ky))
    kyky  = np.ravel(np.power(ky, 2))
    kzkzr = np.ravel(np.power(kzr, 2))
    kzkzt = np.ravel(np.power(kzt, 2))
    kzr   = np.ravel(kzr)
    kzt   = np.ravel(kzt)

    I     = np.identity(2 * kx.size)

    A = np.concatenate([
        np.concatenate([
            -1j * np.diag(np.divide(kxky, kzr)),
            -1j * np.diag(np.divide(kyky + kzkzr, kzr))
        ], axis = 1),
        np.concatenate([
            1j * np.diag(np.divide(kxkx + kzkzr, kzr)),
            1j * np.diag(np.divide(kxky, kzr))
        ], axis = 1)
    ], axis = 0)

    B = np.concatenate([
        np.concatenate([
            1j * np.diag(np.divide(kxky, kzt)),
            1j * np.diag(np.divide(kyky + kzkzt, kzt))
        ], axis = 1),
        np.concatenate([
            -1j * np.diag(np.divide(kxkx + kzkzt, kzt)),
            -1j * np.diag(np.divide(kxky, kzt))
        ], axis = 1)
    ], axis = 0) 

    return np.concatenate([I, A], axis = 0), np.concatenate([I, B], axis = 0)

class HomogenousLayer2D:
    def __init__(self, height, material):
        self.height = height
        self.material = material

    def get_eigenmodes(self, theta, phi, wavelength, r_region, t_region, max_harmonics):
        raise NotImplementedError
class Layer2D:
    def __init__(self, height, texture, material_mapping, period_profile):
        self.height = height
        self.texture = texture
        self.material_mapping = material_mapping
        self.period_profile = period_profile

        self.__convolution_matrix_cache = {}

    def get_field_components_from_incident_wave(self, theta, phi, wavelength, r_region, t_region, max_harmonics):
        kx, ky, kzr, kzt, total_harmonics, harmonic_indices = get_wave_vector_expansion(
            theta, phi, wavelength, self.period_profile, r_region.material, t_region.material, max_harmonics)

        W, V, lam = self.get_eigenmodes(theta, phi, wavelength, r_region, t_region, max_harmonics)

        e_r = np.diag(np.exp(-get_k0(wavelength) * lam * 0)) # identity
        e_t = np.diag(np.exp(-get_k0(wavelength) * lam * self.height))

        kx = np.ravel(kx)
        ky = np.ravel(ky)
        kzr = np.ravel(kzr)
        kzt = np.ravel(kzt)

        kz_inc = get_k0(wavelength) * r_region.material.get_ri(wavelength) * cos(theta)

        # S-Polarization
        s_polarized_source = np.zeros((2 * total_harmonics), dtype=complex)
        s_polarized_source[                  total_harmonics // 2] = 1
        s_polarized_source[total_harmonics + total_harmonics // 2] = 0
        c_s_polarized = np.linalg.solve(W, s_polarized_source)

        E_rx_s_polarized, E_ry_s_polarized = np.split(np.matmul(W, c_s_polarized), 2)
        E_rz_s_polarized                   = -(np.multiply(kx, E_rx_s_polarized) + np.multiply(ky, E_ry_s_polarized)) / kzr
        E_tx_s_polarized, E_ty_s_polarized = np.split(np.matmul(W, np.matmul(e_t, c_s_polarized)), 2)
        E_tz_s_polarized                   = -(np.multiply(kx, E_tx_s_polarized) + np.multiply(ky, E_ty_s_polarized)) / kzr
        
        # P-Polarization
        p_polarized_source = np.zeros((2 * total_harmonics), dtype=complex)
        p_polarized_source[                  total_harmonics // 2] = 0
        p_polarized_source[total_harmonics + total_harmonics // 2] = 1
        c_p_polarized = np.linalg.solve(W, p_polarized_source)

        E_rx_p_polarized, E_ry_p_polarized = np.split(np.matmul(W, c_p_polarized), 2)
        E_rz_p_polarized                   = -(np.multiply(kx, E_rx_p_polarized) + np.multiply(ky, E_ry_p_polarized)) / kzr
        E_tx_p_polarized, E_ty_p_polarized = np.split(np.matmul(W, np.matmul(e_t, c_p_polarized)), 2)
        E_tz_p_polarized                   = -(np.multiply(kx, E_tx_p_polarized) + np.multiply(ky, E_ty_p_polarized)) / kzr

        E_r_s_polarized = np.stack([E_rx_s_polarized, E_ry_s_polarized, E_rz_s_polarized], axis = 1)
        E_r_p_polarized = np.stack([E_rx_p_polarized, E_ry_p_polarized, E_rz_p_polarized], axis = 1)
        E_r_avg         = 0.5 * (E_r_s_polarized + E_r_p_polarized)
        E_t_s_polarized = np.stack([E_tx_s_polarized, E_ty_s_polarized, E_tz_s_polarized], axis = 1)
        E_t_p_polarized = np.stack([E_tx_p_polarized, E_ty_p_polarized, E_tz_p_polarized], axis = 1)
        E_t_avg         = 0.5 * (E_t_s_polarized + E_t_p_polarized)

        E_r_norm_sq     = np.power(np.linalg.norm(E_r_avg, axis = 1), 2)
        E_t_norm_sq     = np.power(np.linalg.norm(E_t_avg, axis = 1), 2)

        R_efficiencies  = -np.real(kzr) * E_r_norm_sq / np.real(kz_inc)
        T_efficiencies  =  np.real(kzt) * E_t_norm_sq / np.real(kz_inc)

        return {
            'reflected': {
                'E': {
                    's_polarized': E_r_s_polarized,
                    'p_polarized': E_r_p_polarized,
                    'avg': E_r_avg,
                    'avg_eff': R_efficiencies
                }
            },
            'transmitted': {
                'E': {
                    's_polarized': E_t_s_polarized,
                    'p_polarized': E_t_p_polarized,
                    'avg': E_t_avg,
                    'avg_eff': T_efficiencies
                }
            }
        }

    def get_ri_profile(self, wavelength):
        """
        Get the permeability/permittivity profile of the layer, by applying the mapped
        materials to the texture.

        NOTE: Assumes that the magnetic permeability is equal to 1, so that n**2 = eps * mu = eps
        """
        profile = np.zeros(self.texture.shape, dtype=complex)
        for material_index, material in self.material_mapping.items():
            ri = material.get_ri(wavelength)
            profile += np.where(self.texture == material_index, ri, 0)
        return profile

    def get_convolution_matrix(self, wavelength, max_harmonics):
        """
        Get the convolution matrix of the permeability/permittivity profile of the layer.
        """
        try:
            return self.__convolution_matrix_cache[(wavelength, max_harmonics)]
        except KeyError:
            max_harmonics_x, max_harmonics_y = max_harmonics
            harmonics_x = np.arange(-max_harmonics_x, max_harmonics_x + 1)
            harmonics_y = np.arange(-max_harmonics_y, max_harmonics_y + 1)
            x, y = np.meshgrid(harmonics_x, harmonics_y)

            yres, xres = self.texture.shape

            cols = np.add.outer(-np.ravel(x), np.ravel(x)) + (xres // 2)
            rows = np.add.outer(-np.ravel(y), np.ravel(y)) + (yres // 2)

            ri_profile = self.get_ri_profile(wavelength)

            eps = np.fft.fftshift(np.fft.fft2(np.power(ri_profile, 2)) / ri_profile.size)[rows, cols]
            self.__convolution_matrix_cache[(wavelength, max_harmonics)] = eps
            
            return eps

    def get_eigenmodes(self, theta, phi, wavelength, r_region, t_region, max_harmonics):
        """
        Calculate and return the eigenmodes of the incident wave after interaction with the layer.

        NOTE: Assumes a relative magnetic permeability equal to unity!!!
        """
        kx, ky, kzr, kzt, total_harmonics, _ = get_wave_vector_expansion(
            theta, phi, wavelength, self.period_profile, 
            r_region.material, t_region.material, max_harmonics)

        kx  = np.diag(np.ravel(kx))
        ky  = np.diag(np.ravel(ky))
        kzr = np.diag(np.ravel(kzr))
        kzt = np.diag(np.ravel(kzt))

        eps    = self.get_convolution_matrix(wavelength, max_harmonics)
        mu     = np.identity(total_harmonics)
        eps_kx = np.linalg.solve(eps, kx)
        eps_ky = np.linalg.solve(eps, ky)

        # Construct the P matrix
        P = np.concatenate([
            np.concatenate([
                np.matmul(kx, eps_ky),
                mu - np.matmul(kx, eps_kx)
            ], axis = 1),
            np.concatenate([
                np.matmul(ky, eps_ky) - mu,
                -np.matmul(ky, eps_kx)
            ], axis = 1)
        ], axis = 0)

        # NOTE: If materials with a mu that varies more by wavelength is to be used, this needs modification
        #       as shown in the comments behind each line.
        mu_kx = kx # np.linalg.solve(mu, kx)
        mu_ky = ky # np.linalg.solve(mu, ky)
        
        # Construct the Q matrix
        Q = np.concatenate([
            np.concatenate([
                np.matmul(kx, mu_ky),
                eps - np.matmul(kx, mu_kx)
            ], axis = 1),
            np.concatenate([
                np.matmul(ky, mu_ky) - eps,
                -np.matmul(ky, mu_kx)
            ], axis = 1)
        ], axis = 0)

        lamsq, W = np.linalg.eig(np.matmul(P, Q))
        lam = np.sqrt(lamsq)
        V = np.matmul(Q, np.matmul(W, np.diag(1 / lam)))

        return W, V, lam

    def get_wave_vector_expansion(self, theta, phi, wavelength, r_region, t_region, max_harmonics):
        return get_wave_vector_expansion(theta, phi, wavelength, self.period_profile, r_region.material, t_region.material, max_harmonics)

def draw_rectangle(x_fill, y_fill, resolution):
    xres, yres = resolution
    x, y = np.meshgrid(np.linspace(0, 1, xres), np.linspace(0, 1, yres))

    return (np.logical_and(x < x_fill, y < y_fill)).astype(int)

def draw_circle(fill, resolution):
    xres, yres = resolution
    x, y = np.meshgrid(np.linspace(0, 1, xres // 2), np.linspace(1, 0, yres // 2))

    q1 = np.power(x, 2) + np.power(y, 2) < fill * fill
    q12 = np.concatenate([np.flip(q1, axis = 1), q1], axis = 1)

    return np.concatenate([q12, np.flip(q12, axis = 0)], axis = 0)